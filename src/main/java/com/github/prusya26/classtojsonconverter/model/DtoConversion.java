package com.github.prusya26.classtojsonconverter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
public class DtoConversion {
    @XmlAttribute
    private String id;
    @XmlAttribute
    private String name;
    private DtoClass dtoClass;
    @XmlAttribute
    private Type type;
    @XmlElementWrapper(name = "savedInstances")
    @XmlElement(name = "savedInstance")
    private List<SavedDtoInstance> savedInstances;

    @XmlEnum
    public enum Type {
        OBJECT,
        ARRAY
    }
}
