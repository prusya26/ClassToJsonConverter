package com.github.prusya26.classtojsonconverter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dtoClass")
public class DtoClass {
    @XmlAttribute
    private String id;
    @XmlAttribute
    private String className;
    @XmlAttribute
    private String pathToClass;
    @XmlElementWrapper(name = "dependencies")
    @XmlElement(name = "dtoClass")
    private List<DtoClass> dependencies;
}
