package com.github.prusya26.classtojsonconverter.controller;

import com.github.prusya26.classtojsonconverter.DataStorage;
import com.github.prusya26.classtojsonconverter.Main;
import com.github.prusya26.classtojsonconverter.model.Conversions;
import com.github.prusya26.classtojsonconverter.model.DtoConversion;
import com.github.prusya26.classtojsonconverter.tools.DialogUtils;
import com.github.prusya26.classtojsonconverter.tools.WindowUtils;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MainController implements Initializable {
    public Button newConversionBtn;
    public ListView<ConversionItem> conversionsListView;
    public TabPane conversionsTabPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Conversions conversions = DataStorage.getInstance().getConversions();
        conversionsListView.setItems(FXCollections.observableArrayList(conversions.getConversions()
                .stream().map(c -> ConversionItem.builder()
                        .conversion(c)
                        .build())
                .collect(Collectors.toList())));
    }

    public void onNewConversion(ActionEvent event) {
        try {
            WindowUtils.showModalWindow(getClass().getResource("/fxml/conversion_creation.fxml"),
                    "Create new conversion", null, null);
        } catch (IOException e) {
            DialogUtils.showExceptionDialog(Main.APP_NAME, "Window creation error", null, e);
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    private static class ConversionItem {
        private DtoConversion conversion;
        private ConversionItemController itemController;
    }
}
