package com.github.prusya26.classtojsonconverter.controller;

import com.github.prusya26.classtojsonconverter.DataStorage;
import com.github.prusya26.classtojsonconverter.Main;
import com.github.prusya26.classtojsonconverter.tools.ClassFileParser;
import com.github.prusya26.classtojsonconverter.tools.DialogUtils;
import com.google.common.reflect.ClassPath;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class ConversionCreationController implements Initializable {
    public TableView<ClassInfoItem> filesTableView;
    public TableColumn<ClassInfoItem, Boolean> markCol;
    public TableColumn<ClassInfoItem, String> classNameCol;
    public TableColumn<ClassInfoItem, String> classFilePathCol;
    public TableColumn<ClassInfoItem, Boolean> mainClassCol;
    public TextField conversionNameEdt;
    public CheckBox copyClassesCheckBox;
    public Button saveBtn;
    public Button cancelBtn;
    public TextField classFilesDirEdt;
    public Button selectClassFilesDirBtn;
    public Button loadClassFilesInfoBtn;
    public ProgressIndicator progressIndicator;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        filesTableView.setItems(FXCollections.observableArrayList());
        markCol.setCellFactory(CheckBoxTableCell.forTableColumn(markCol));
        markCol.setCellValueFactory(new PropertyValueFactory<>("marked"));
        classNameCol.setCellValueFactory(new PropertyValueFactory<>("className"));
        classFilePathCol.setCellValueFactory(new PropertyValueFactory<>("packageName"));
        mainClassCol.setCellFactory(CheckBoxTableCell.forTableColumn(mainClassCol));
        mainClassCol.setCellValueFactory(new PropertyValueFactory<>("mainDto"));
    }

    public void onSaveAction(ActionEvent event) {
        ((Stage) saveBtn.getScene().getWindow()).close();
    }

    public void onCancelAction(ActionEvent event) {
        ((Stage) cancelBtn.getScene().getWindow()).close();
    }

    public void onSelectClassFilesDirBtnAction(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select DTO class files directory");
        DataStorage dataStorage = DataStorage.getInstance();
        String lastDir = dataStorage.getMainProperties().getProperty(DataStorage.KEY_LAST_CLASS_FILES_DIRECTORY);
        if(lastDir != null && !lastDir.isEmpty()) {
            File file = new File(lastDir);
            if(file.exists()) {
                directoryChooser.setInitialDirectory(file);
            }
        }
        File dir = directoryChooser.showDialog(selectClassFilesDirBtn.getScene().getWindow());
        if(dir != null) {
            classFilesDirEdt.setText(dir.getPath());
            dataStorage.getMainProperties().setProperty(DataStorage.KEY_LAST_CLASS_FILES_DIRECTORY, classFilesDirEdt.getText());
        }
    }

    public void onLoadClassFilesInfoAction(ActionEvent event) {
        if(!classFilesDirEdt.getText().isEmpty()) {
            File dir = new File(classFilesDirEdt.getText());
            if(dir.exists() && dir.isDirectory()) {
                //Все хорошо, можно загружать классы
                Task<List<ClassPath.ClassInfo>> loadClassInfoTask = new Task<List<ClassPath.ClassInfo>>() {
                    @Override
                    protected List<ClassPath.ClassInfo> call() throws Exception {
                        return ClassFileParser.parseDirectory(dir);
                    }
                };
                loadClassInfoTask.setOnSucceeded(e -> {
                    setProgressState(false);
                    filesTableView.getItems().addAll(loadClassInfoTask.getValue()
                            .stream()
                            .map(ClassInfoItem::new)
                            .collect(Collectors.toList()));
                    filesTableView.getItems().forEach(i -> i.mainDtoProperty().addListener(new ChangeListener<Boolean>() {
                        @Override
                        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                            if(newValue != null && newValue) {
                                filesTableView.getItems().stream()
                                        .filter(classInfoItem -> !classInfoItem.equals(i))
                                        .forEach(classInfoItem -> classInfoItem.setMainDto(false));
                            }
                        }
                    }));
                });
                loadClassInfoTask.setOnFailed(e -> {
                    setProgressState(false);
                    DialogUtils.showExceptionDialog(Main.APP_NAME, "Error",
                            null, (Exception) loadClassInfoTask.getException());
                });
                setProgressState(true);
                Thread t = new Thread(loadClassInfoTask);
                t.start();
            } else {
                DialogUtils.showInfoDialog(Main.APP_NAME,
                        "Wrong path to class files directory", null);
            }
        }
    }

    private void setProgressState(boolean inProgress) {
        progressIndicator.setVisible(inProgress);
        filesTableView.setDisable(inProgress);
        conversionNameEdt.setDisable(inProgress);
        copyClassesCheckBox.setDisable(inProgress);
        saveBtn.setDisable(inProgress);
        cancelBtn.setDisable(inProgress);
        classFilesDirEdt.setDisable(inProgress);
        selectClassFilesDirBtn.setDisable(inProgress);
        loadClassFilesInfoBtn.setDisable(inProgress);
    }

    public static class ClassInfoItem {
        private SimpleBooleanProperty marked;
        private SimpleBooleanProperty mainDto;
        private SimpleStringProperty className;
        private SimpleStringProperty packageName;
        private ClassPath.ClassInfo dtoClass;

        public ClassInfoItem(ClassPath.ClassInfo classInfo) {
            this.dtoClass = classInfo;
            marked = new SimpleBooleanProperty(false);
            mainDto = new SimpleBooleanProperty(false);
            className = new SimpleStringProperty(classInfo.getSimpleName());
            packageName = new SimpleStringProperty(classInfo.getPackageName());
        }

        public boolean isMarked() {
            return marked.get();
        }

        public SimpleBooleanProperty markedProperty() {
            return marked;
        }

        public void setMarked(boolean marked) {
            this.marked.set(marked);
        }

        public boolean isMainDto() {
            return mainDto.get();
        }

        public SimpleBooleanProperty mainDtoProperty() {
            return mainDto;
        }

        public void setMainDto(boolean mainDto) {
            this.mainDto.set(mainDto);
        }

        public String getClassName() {
            return className.get();
        }

        public SimpleStringProperty classNameProperty() {
            return className;
        }

        public void setClassName(String className) {
            this.className.set(className);
        }

        public String getPackageName() {
            return packageName.get();
        }

        public SimpleStringProperty packageNameProperty() {
            return packageName;
        }

        public void setPackageName(String packageName) {
            this.packageName.set(packageName);
        }

        public ClassPath.ClassInfo getDtoClass() {
            return dtoClass;
        }

        public void setDtoClass(ClassPath.ClassInfo dtoClass) {
            this.dtoClass = dtoClass;
        }
    }
}
