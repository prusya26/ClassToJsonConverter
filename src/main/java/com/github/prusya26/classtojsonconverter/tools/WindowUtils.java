package com.github.prusya26.classtojsonconverter.tools;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Pair;

import java.io.IOException;
import java.net.URL;

public class WindowUtils {

    public interface OnBeforeStageCreation <T> {
        void handle(T controller);
    }

    public static <T> Pair<Stage, T> createStage(URL fxmlLocation,
                                                 OnBeforeStageCreation<T> onBeforeStageCreation) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlLocation);
        Parent root = fxmlLoader.load();
        T controller = fxmlLoader.getController();
        if(onBeforeStageCreation != null) onBeforeStageCreation.handle(controller);
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        return new Pair<>(stage, controller);
    }

    public static <T> T showModalWindow(URL fxmlLocation, String title, Window owner, OnBeforeStageCreation<T> onBeforeStageCreation) throws IOException {
        Pair<Stage, T> stagePair = createStage(fxmlLocation, onBeforeStageCreation);
        Stage stage = stagePair.getKey();
        stage.setTitle(title);
        stage.initOwner(owner);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        return stagePair.getValue();
    }
}
