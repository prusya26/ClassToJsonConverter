package com.github.prusya26.classtojsonconverter.tools;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;

import java.io.PrintWriter;
import java.io.StringWriter;

public class DialogUtils {

    private static Alert createAlert(Alert.AlertType alertType, String title, String headerText, String contentText) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.initModality(Modality.APPLICATION_MODAL);
        return alert;
    }

    public static Alert createInfoDialog(String title, String headerText, String contentText) {
        return createAlert(Alert.AlertType.INFORMATION, title, headerText, contentText);
    }

    public static Alert createWarningDialog(String title, String headerText, String contentText) {
        return createAlert(Alert.AlertType.WARNING, title, headerText, contentText);
    }

    public static Alert createErrorDialog(String title, String headerText, String contentText) {
        return createAlert(Alert.AlertType.ERROR, title, headerText, contentText);
    }

    public static Alert createExceptionDialog(String title, String headerText, String contentText, Exception e) {
        Alert alert = createAlert(Alert.AlertType.ERROR, title, headerText, contentText);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        return alert;
    }

    public static Alert createConfirmDialog(String title, String headerText, String contentText) {
        return createAlert(Alert.AlertType.CONFIRMATION, title, headerText, contentText);
    }

    public static void showInfoDialog(String title, String headerText, String contentText) {
        Alert infoDialog = createInfoDialog(title, headerText, contentText);
        infoDialog.showAndWait();
    }

    public static void showWarningDialog(String title, String headerText, String contentText) {
        Alert warningDialog = createWarningDialog(title, headerText, contentText);
        warningDialog.showAndWait();
    }

    public static void showErrorDialog(String title, String headerText, String contentText) {
        Alert errorDialog = createErrorDialog(title, headerText, contentText);
        errorDialog.showAndWait();
    }

    public static void showExceptionDialog(String title, String headerText, String contentText, Exception e) {
        Alert exceptionDialog = createExceptionDialog(title, headerText, contentText, e);
        exceptionDialog.showAndWait();
    }
}
