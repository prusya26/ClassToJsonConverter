package com.github.prusya26.classtojsonconverter.tools;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.stream.Collectors;

public class ClassFileParser {
    public static List<ClassPath.ClassInfo> parseDirectory(File dir) throws IOException {
        ClassPath classPath = ClassPath.from(new URLClassLoader(new URL[] { dir.toURI().toURL()}));
        ImmutableSet<ClassPath.ClassInfo> allClasses = classPath.getAllClasses();
        String dirPath = dir.getPath().replace('\\', '/');
        return allClasses.stream().filter(classInfo -> {
            String path = classInfo.url().getPath();
            return path.contains(dirPath) && !classInfo.getPackageName().isEmpty();
        }).collect(Collectors.toList());
    }
}
