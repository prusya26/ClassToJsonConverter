package com.github.prusya26.classtojsonconverter;

import com.github.prusya26.classtojsonconverter.model.Conversions;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class DataStorage {

    private static final String CONFIGURATION_FILE_NAME = "configuration.properties";

    public static final String KEY_DATAFILE = "datafile";
    public static final String KEY_LAST_CLASS_FILES_DIRECTORY = "last-class-files-directory";

    private static final String DEFAULT_DATAFILE = "data.xml";

    private final Properties mainProperties;
    private Conversions conversions;

    private static DataStorage ourInstance = new DataStorage();

    public static DataStorage getInstance() {
        return ourInstance;
    }

    private DataStorage() {

        //load main properties
        mainProperties = new Properties();
        try {
            mainProperties.load(new FileInputStream(CONFIGURATION_FILE_NAME));
        } catch (IOException e) {
            setDefaults();
        }
    }

    public Conversions getConversions() {
        if(conversions == null) {
            try {
                loadDataFile();
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        return conversions;
    }

    public void save() throws IOException, JAXBException {
        mainProperties.store(new FileWriter(CONFIGURATION_FILE_NAME), null);

        JAXBContext jaxbContext = JAXBContext.newInstance(Conversions.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(conversions, new File(mainProperties.getProperty(KEY_DATAFILE)));
    }

    public Properties getMainProperties() {
        return mainProperties;
    }

    private void setDefaults() {
        mainProperties.setProperty(KEY_DATAFILE, DEFAULT_DATAFILE);
    }

    private void loadDataFile() throws JAXBException {
        File dataFile = new File(mainProperties.getProperty(KEY_DATAFILE));
        if(dataFile.exists()) {
            JAXBContext jaxbContext = JAXBContext.newInstance(Conversions.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            conversions = (Conversions) unmarshaller.unmarshal(dataFile);
            if(conversions.getConversions() == null) {
                conversions.setConversions(new ArrayList<>());
            }
        } else {
            conversions = new Conversions(new ArrayList<>());
        }
    }
}
